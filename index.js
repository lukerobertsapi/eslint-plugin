'use strict';

module.exports = {
    configs: {
        node: require('./configs/node'),
        webpack: require('./configs/webpack'),
    },
    rules: {
        "leftover-only": require("./rules/leftover-only"),
    },
};
